const PATH = require('path');
const express = require('express');
const router = express.Router();
const fs = require('fs');

router.get('/', function (req, res, next) {
    fs.readdir("videos", (err, files) => {
        files.splice(files.findIndex((file) => file === '.gitignore'), 1)
        res.json(files)
    });
});

router.get('/:id', function (req, res, next) {
    const path = `videos/${req.params.id}`;
    const {size} = fs.statSync(path);
    const ext = PATH.extname(path);
    const range = req.headers.range;
    if (range) { // si la requete crée par le lecteur contient un attribut range on divise la reponse en plusieurs blocs
        const parts = range.replace(/bytes=/, "").split("-");
        const start = parseInt(parts[0], 10);
        const end = parts[1]
            ? parseInt(parts[1], 10)
            : size - 1;
        const file = fs.createReadStream(path, {start, end});
        const head = {
            'Content-Range': `bytes ${start}-${end}/${size}`,
            'Accept-Ranges': 'bytes',
            'Content-Length': (end - start) + 1,
            'Content-Type': 'video/mp4',
        };
        res.writeHead(206, head);
        file.pipe(res);
    } else {
        const head = {
            'Content-Length': size,
            'Content-Type': `video/${ext}`,
        };
        res.writeHead(200, head);
        fs.createReadStream(path).pipe(res)
    }
});

module.exports = router;