function loadVideo(name) {
    let url = "http://localhost:3000/videos";
    let video = document.getElementById('video');
    let videoSource = document.getElementById('videoSource');
    videoSource.src = `${url}/${name}`;
    // console.log(name.split('.')[-1])
    // videoSource.type = name.split('.')[-1]
    video.load();
}


fetch('http://localhost:3000/videos')
    .then((data) => data.json())
    .then((dataJson) => {
        const videos = dataJson
        const buttonContainer = document.getElementById('buttonContainer')
        videos.forEach(video => {
            let button = document.createElement('button')
            button.type = "button"
            button.classList.add("btn", "btn-primary")
            button.onclick = () => loadVideo(video)
            button.innerText = video
            buttonContainer.appendChild(button)
        });
    })