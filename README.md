COMMENT INITIALISER NOTRE PROJET NODE :

1- Cloner le repo 

2- Une fois le repo cloné, ouvrir un terminal PowerShell à la racine du projet

3- Taper la commande : 'npm install'

4- Mettre les videos dans le dossier videos

5- Taper la commande : 'npm start'

6- Aller sur votre navigateur internet et tapez : 'localhost:3000'
